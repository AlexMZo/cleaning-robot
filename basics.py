#!/usr/env/python3

# cr: cleaning robot

import tkinter as tk
from PIL import Image, ImageTk
from tkinter.ttk import Label


def load_image(pre_image):
    image = ImageTk.PhotoImage(pre_image)
    return(image)

def create_label(cr, path, x, y):
    image = load_image(path)
    label = Label(cr, image=image)
    label.image = image
    label.place(x=x, y=y)
    return (label)

def set_basics(cr):
    cr.test = "hello"
    cr.master.title("Cleaning Robot DSA")
    cr.screen_width = cr.master.winfo_screenwidth()
    cr.screen_height = cr.master.winfo_screenheight()
    cr.master.geometry("%dx%d+0+0" % (cr.screen_width, cr.screen_height))

def setup_reset_button(cr):
    cr.reset_button = tk.Button(cr.master, text="Reset", command=cr.reset)
    cr.reset_button.place(x=1600, y=100)

def set_images(cr):
    cr.room_image = Image.open("./images/Room_BW_small.bmp")
    cr.room_pixels = list(cr.room_image.getdata())
    cr.room = create_label(cr, cr.room_image, 0, 0)
    cr.robot_departure_image = Image.open("./images/robot_BW_small.bmp")
    cr.robot_departure_pixels = list(cr.robot_departure_image.getdata())
    cr.robot_departure = create_label(cr, cr.robot_departure_image, 1600, 300)
    cr.robot_arrival_image = Image.open("./images/robot_BW_small.bmp")
    cr.robot_arrival_pixels = list(cr.robot_arrival_image.getdata())
    cr.robot_arrival = create_label(cr, cr.robot_arrival_image, 1600, 500)

def add_motion_robots(cr):
    cr.robot_departure.bind("<B1-Motion>", cr.drag_departure_label)
    cr.robot_arrival.bind("<B1-Motion>", cr.drag_arrival_label)

def init_texts(cr):
    cr.starting_point_text = tk.Label(cr, text="Starting point robot",
                         wraplength = 100,
                         justify = tk.CENTER)
    cr.starting_point_text.place(x=1578, y=250)
    cr.arrival_point_text = tk.Label(cr, text="Arrival point robot",
                         wraplength = 100,
                         justify = tk.CENTER)
    cr.arrival_point_text.place(x=1585, y=450)
    cr.collision_text = tk.Label(cr, text="",
                         wraplength = 100,
                         justify = tk.CENTER)
    cr.collision_text.place(x=1578, y=180)
    cr.collision_text.config(font=("Courier", 12))
