#!/usr/env/python3

import tkinter as tk
from PIL import Image, ImageTk
from tkinter import filedialog
from tkinter import *
from tkinter.ttk import Frame, Label, Style
import pyautogui
import math
from basics import *
from collision import *
import random
from dijkstar import Graph, find_path
import time

# cr: cleaning robot
# robot picture size: 48x48
# plan picture size: 1350x980


def init_scroll(cr):
    horizontal_bar_image = Image.open("./images/horizontal_bar.png")
    cr.horizontal_bar = create_label(cr, horizontal_bar_image, 1440, 700)
    scroll_slider_image = Image.open("./images/scroll_slider.png")
    cr.scroll_button = create_label(cr, scroll_slider_image, 1450, 685)
    cr.scroll_button.bind("<B1-Motion>", cr.drag_slider_label)

def check_robot_size(cr):
    y = 0
    while (y < 48):
        x = 0
        while (x < 48):
            x = x + 1
        y = y + 1
    
def check_available_space(cr):
    y = 0
    limit_y = 980 - 48
    limit_x = 1350 - 48
    while (y < limit_y):
        x = 0
        while (x < limit_x):
            x = x + 1
        y = y + 1
    
class cleaning_robot(tk.Frame):

    def __init__(self, parent):
        super(cleaning_robot, self).__init__(parent)
        self.parent = parent
        set_basics(self)
        set_images(self)
        setup_reset_button(self)
        add_motion_robots(self)
        init_texts(self)
        init_scroll(self)
        self.samples = []
        self.nb_samples = 1000
        self.random_sample()
        self.samp = tk.StringVar()
        self.sampEntered = tk.Entry(self, width = 15, textvariable = self.samp)
        self.sampEntered.place(x=1700, y = 900)
        self.sampleButton = tk.Button(self, text="3: generate samples & GO !", command=self.acceptSamples)
        self.sampleButton.place(x=1700, y = 920)
        self.rad = tk.StringVar()
        self.radEntered = tk.Entry(self, width = 15, textvariable = self.rad)
        self.radEntered.place(x=1550, y = 900)
        self.radiusButton = tk.Button(self, text="2: go for radius", command=self.acceptRadius)
        self.radiusButton.place(x=1550, y = 920)
        self.radius = 300
        self.it = tk.StringVar()
        self.itEntered = tk.Entry(self, width = 15, textvariable = self.it)
        self.itEntered.place(x=1400, y = 900)
        self.iterButton = tk.Button(self, text="1: nb iter", command=self.acceptIter)
        self.iterButton.place(x=1400, y = 920)
        self.nb_iter = 1
        self.pointsBetweenNodes = []
        
        # self.room_pixels_config = [()]
        # self.setup_config_button()
        # self.config_displayed = False

    def random_sample(self):
        i = 0
        self.samples = []
        while i < self.nb_samples:
            self.samples.append((random.randint(50, 940), random.randint(50, 1300)))
            i = i + 1

    def update_the_slider(self, pos1, pos2):
        self.robot_departure.place(x=pos1, y=pos2)

    def getNearestEdges(self):
        self.edges = []
        self.pointsList = []
        i = 0
        for elem in self.samples:
            for elem2 in self.samples:
                if elem == elem2:
                    continue
                dist = int(self.calculate_distance(elem, elem2))
                if dist < self.radius:
                    resol = max(abs(elem[1] - elem2[1]), abs(elem[0] - elem2[0]))
                    middle = (elem2[1] - elem[1], elem2[0] - elem[0])#xy
                    points = []
                    i = 1
                    while i < resol:
                        ro = (int((float(i/resol)*middle[0])),int((float(i/resol)*middle[1])))#x au début 
                        point = (elem[1]+ro[0], elem2[0]+ro[1])
                        points.append(point)
                        i = i + 1
                    self.pointsList.append(points)
                    if check_collision_line(self, points):
                        continue 
                    res = [elem, elem2, dist]
                    self.edges.append(res)
        self.goDijkstra()
        # on a besoin du point de départ et d'arrivée du robot
        # edges = []
        # for elem in self.samples:
    
        
    def display_it(self):
        self.backupInit = self.cInit
        self.backupGoal = self.cGoal
        j = 0
        while j < len(self.djiRes.nodes) - 1:
            dist = self.djiRes.edges[j]
            resol = max(abs(self.revCorresp[self.djiRes.nodes[j]][1] - self.revCorresp[self.djiRes.nodes[j+1]][1]), abs(self.revCorresp[self.djiRes.nodes[j]][0] - self.revCorresp[self.djiRes.nodes[j+1]][0]))
            middle = (self.revCorresp[self.djiRes.nodes[j+1]][1] - self.revCorresp[self.djiRes.nodes[j]][1], self.revCorresp[self.djiRes.nodes[j+1]][0] - self.revCorresp[self.djiRes.nodes[j]][0])#xy
            points = []
            i = 1
            while i < resol:
                ro = (int((float(i/resol)*middle[0])),int((float(i/resol)*middle[1])))#x au début 
                point = (self.revCorresp[self.djiRes.nodes[j]][1]+ro[0], self.revCorresp[self.djiRes.nodes[j+1]][0]+ro[1])
                points.append(point)
                i = i + 1
            self.pointsBetweenNodes.append(points)
            j = j + 1
        self.final_list = []
        i = 0
        while i < len(self.pointsBetweenNodes):
            for elem in self.pointsBetweenNodes[i]:
                self.final_list.append(elem)
            i = i + 1
        print("TOTAL LEN : :::::::::::::::::::::::: ", len(self.final_list))
        
    
    def goDijkstra(self):
        graph = Graph()
        self.copyEdges = []
        self.backup = []
        self.corresp = {}
        self.revCorresp = {}
        i = 0
        for sample in self.samples:
            if sample not in self.corresp:
                self.corresp[sample] = i
                self.revCorresp[i] = sample
                i = i + 1
        # print(self.edges)
        # print(self.corresp)
        
        for edge in self.edges:
            graph.add_edge(self.corresp[edge[0]], self.corresp[edge[1]], edge[2])
            #print("corresp edge 0 : ", self.corresp[edge[0]])
            #print("corresp edge 1 : ", self.corresp[edge[1]])
            #print("edge 2 : ", edge[2])
        self.djiRes = find_path(graph, self.corresp[self.cInit], self.corresp[self.cGoal])
        self.display_it()
        return
    
    def calculate_distance(self, node1, node2):
        y_diff = abs(node1[0] - node2[0])
        x_diff = abs(node1[1] - node2[1])
        return math.hypot(y_diff, x_diff)
        
    def go(self):
        self.placeBaseRobots()
        self.getNearestEdges()
        return
        
    def acceptSamples(self):
        self.nb_samples = int(self.samp.get())
        print("New value of self.samples : ", self.nb_samples)
        self.random_sample()
        self.removeCollisionSample()
        self.go()
        

    def acceptRadius(self):
        self.radius = int(self.rad.get())
        print("New value of self.radius : ", self.radius)

    def acceptIter(self):
        self.nb_iter = int(self.it.get())
        print("New value of self.nb_iter : ", self.nb_iter)

    def removeCollisionSample(self):
        print("first len : ", len(self.samples))
        for s in self.samples:
            if check_collision_samples(self, s):
                self.samples.remove(s)
        self.nb_samples = len(self.samples)
        print("second len : ", len(self.samples))
        print(self.samples)
        

    def drag_departure_label(self, event):
        self.robot_departure_x = pyautogui.position().x
        self.robot_departure_y = pyautogui.position().y - 65
        self.robot_departure.place(x=self.robot_departure_x, y=self.robot_departure_y)
        check_collision(self, self.robot_departure_x, self.robot_departure_y)

    def drag_arrival_label(self, event):
        self.robot_arrival_x = pyautogui.position().x
        self.robot_arrival_y = pyautogui.position().y - 65
        self.robot_arrival.place(x=self.robot_arrival_x, y=self.robot_arrival_y)
        check_collision(self, self.robot_arrival_x, self.robot_arrival_y)

    def drag_slider_label(self, event):
        slider_x = pyautogui.position().x
        pos_slider_x = 0
        slider_y = 685
        if slider_x > 1780:
            slider_x = 1780
        if slider_x < 1450:
            slider_x = 1450
        self.scroll_button.place(x=slider_x, y=slider_y)
        if self.final_list == []:
            return
        else:
            len_list = len(self.final_list)
            total = 1780 - 1450 # 330
            pos_slider_x = int((1780 - slider_x) / (330/len_list))
            # div = 330 / len_list
            idx = len_list - pos_slider_x
            if idx < 0:
                return
            if idx > len_list:
                return
            self.robot_departure_x = self.final_list[idx][0]
            self.robot_departure_y = self.final_list[idx][1]
            self.robot_departure.place(x=self.robot_departure_x, y=self.robot_departure_y)   
        

    def reset(self):
        self.robot_departure.place(x=1600, y=300)
        self.robot_arrival.place(x=1600, y=500)
        self.collision_text['text'] = ""
        self.final_list.clear()
        self.pointsBetweenNodes.clear()


    def setup_config_button(self):
        self.config_button = tk.Button(self.master, text="See configuration", command=self.config)
        self.config_button.place(x=1570, y=50)

    def display_config(self):
        y = 0
        while (y < 930):
            x = 0
            while (x < 1300):
                if (self.room_pixels_config[x + (y * 1350)] == (0, 255, 0)):
                    self.img.put("#00FF00", (x, y*1350))
                else:
                    self.img.put("#FF0000", (x,y*1350))
                x = x + 1
            y = y + 1
        
        
    # def config(self):
    #     if (self.room_pixels_config != [()] and self.config_displayed == False):
    #         # print (self.room_pixels_config)
    #         # self.canvas_cspace.destroy()
    #         self.config_displayed = True
    #         return (self.room_pixels_config)
    #     # elif (self.room_pixels_config != [()] and self.config_displayed == True):
    #     #     self.canvas_cspace = Canvas(self.parent, width=1350, height=980)
    #     #     self.canvas_cspace.pack()
    #     #     self.img = PhotoImage(width=1350, height=980)
    #     #     self.canvas_cspace.create_image((1350, 980), image=self.img, state="normal")
    #     #     self.display_config()
    #     #     self.config_displayed = False
    #     self.room_pixels_config = [()]  
    #     self.img = PhotoImage(width=1350, height=980)
    #     # self.canvas_cspace.create_image((1350, 980), image=self.img, state="normal")
    #     y = 0
    #     while (y < 930):
    #         x = 0
    #         while (x < 1300):
    #             if (check_collision_cspace(self, x, y)):
    #                 self.room_pixels_config.append((0, 255, 0))
    #             else:
    #                 self.room_pixels_config.append((0, 0, 0))
    #             x = x + 1
    #         y = y + 1
    #     print(len(self.room_pixels_config))
    #     self.display_config()
    #     self.labelTest = Label(self, image=self.img)
    #     self.labelTest.image = self.img
    #     self.labelTest.place(0,0)
    #     self.config_displayed = False
    #     return (self.room_pixels_config)

    
    def placeBaseRobots(self):
        i = 0
        final = 100000
        final_index = 0
        for elem in self.samples:
            res = int(self.calculate_distance(elem, (self.robot_departure_y, self.robot_departure_x)))
            if res < final:
                final = res
                final_index = i
            i = i + 1
        self.final_dep_idx = final_index
        print("final sample: ", self.samples[final_index])
        self.robot_departure_y = self.samples[final_index][0]
        self.robot_departure_x = self.samples[final_index][1]
        self.robot_departure.place(x=self.robot_departure_x, y=self.robot_departure_y)
        self.cInit = (self.robot_departure_y, self.robot_departure_x)# cInit
        i = 0
        final = 100000
        final_index = 0
        for elem in self.samples:
            res = int(self.calculate_distance(elem, (self.robot_arrival_y, self.robot_arrival_x)))
            if res < final:
                final = res
                final_index = i
            i = i + 1
        self.final_arrival_idx = final_index
        print("final index : ", final_index)
        self.robot_arrival_y = self.samples[final_index][0]
        self.robot_arrival_x = self.samples[final_index][1]
        self.robot_arrival.place(x=self.robot_arrival_x, y=self.robot_arrival_y)# cGoal
        self.cGoal = (self.robot_arrival_y, self.robot_arrival_x)# cGoal


        
    # implement bridge ? Steps:  
    # generate random samples,
    # check if it's in collision,
    # if it is the case, calculate another point near it, thanks to the normal distribution, a derivation of 1 and so on
    # calculate mid points of it


    
if __name__ == "__main__":
    root = tk.Tk()
    main = cleaning_robot(root)
    main.pack(fill="both", expand=True)
    root.mainloop()
