#!/usr/env/python3

import sys
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import QMainWindow, QLabel, QGridLayout, QWidget
from PyQt5.QtCore import QSize
from PyQt5.QtWidgets import QApplication, QWidget, QLabel
from PyQt5.QtGui import QIcon, QPixmap

from PyQt5.QtWidgets import QWidget

# PLAN : Il faut afficher plein de choses sur l'UI :
#
# L'image de la salle avec un petit texte "<size of the room> px"
# L'image du robot avce un petit texte "<size of the robot> px"
# 
#
#

#
#
#

#
# class MainWindow(QMainWindow):
#
#     def __init__(self, screen_size):
#         super().__init__()
#         self.left = 0
#         self.top = 0
#         self.width = screen_size.width()
#         self.height = screen_size.height()
#         self.screen_size = screen_size
#         self.setGeometry(self.left, self.top, self.width, self.height)
#         self.setWindowTitle("Cleaning Robot")
#         self.init_ui()
#
#     def init_ui(self):
#         label = QLabel('plan', self)
#         label.setGeometry(self.left, self.top, self.width, self.height)
#         pre_pixmap = QPixmap('Room.bmp')
#         print("pre_pixmap size", pre_pixmap.size())
#         print("pre_pixmap height", pre_pixmap.size().height())
#         print("pre_pixmap width", pre_pixmap.size().width())
#         pixmap = pre_pixmap.scaled(self.width * 1, self.height * 1, QtCore.Qt.KeepAspectRatio)
#         diff_height = pre_pixmap.size().height() / pixmap.size().height()
#         diff_width = pre_pixmap.size().width() / pixmap.size().width()
#         print("height diff = ", diff_height)
#         print("width diff = ", diff_width)
#         label.setPixmap(pixmap)
#         print("taille du plan = ", pixmap.size())
#
#         label2 = QLabel('robot', self)
#         label2.setGeometry(self.left, self.top, self.width, self.height)
#         pre_pixmap2 = QPixmap('robot.bmp')
#         print("pre_pixmap2 size", pre_pixmap2.size())
#         print("pre_pixmap2 height", pre_pixmap2.size().height())
#         print("pre_pixmap2 width", pre_pixmap2.size().width())
#         pixmap2 = pre_pixmap2.scaled(pre_pixmap2.size().width() / diff_width, pre_pixmap2.size().height() / diff_height, QtCore.Qt.KeepAspectRatio)
#         label2.setPixmap(pixmap2)
#         label2.move(self.width * 0.8, -(self.height * 0.03))
#
#         label3 = QLabel('departure', self)
#         label3.setGeometry(self.left, self.top, self.width, self.height)
#         pre_pixmap3 = QPixmap('departure_flag.png')
#         print("pre_pixmap3 size", pre_pixmap3.size())
#         print("pre_pixmap3 height", pre_pixmap3.size().height())
#         print("pre_pixmap3 width", pre_pixmap3.size().width())
#         pixmap3 = pre_pixmap3.scaled(pre_pixmap3.size().width() / 10, pre_pixmap3.size().height() / 10, QtCore.Qt.KeepAspectRatio)
#         label3.setPixmap(pixmap3)
#         label3.move(self.width * 0.8, -(self.height * 0.3))
#
#         label4 = QLabel('arrival', self)
#         label4.setGeometry(self.left, self.top, self.width, self.height)
#         pre_pixmap4 = QPixmap('arrival_flag.png')
#         print("pre_pixmap4 size", pre_pixmap4.size())
#         print("pre_pixmap4 height", pre_pixmap4.size().height())
#         print("pre_pixmap4 width", pre_pixmap4.size().width())
#         pixmap4 = pre_pixmap4.scaled(pre_pixmap4.size().width() / 10, pre_pixmap4.size().height() / 10, QtCore.Qt.KeepAspectRatio)
#         label4.setPixmap(pixmap4)
#         label4.move(self.width * 0.8, -(self.height * 0.15))
#
#
#
# if __name__ == "__main__":
#     app = QtWidgets.QApplication(sys.argv)
#     screen_size = app.primaryScreen().size()
#     mainWin = MainWindow(screen_size)
#     mainWin.show()
#     sys.exit(app.exec_())
#
