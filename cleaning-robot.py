#!/usr/env/python3

import tkinter as tk
from PIL import Image, ImageTk
from tkinter import filedialog
from tkinter import ttk
from tkinter.ttk import Frame, Label, Style
import pyautogui


#Horrible. Refactor

class CleaningRobot(tk.Frame):
    def __init__(self, parent):
        super(CleaningRobot, self).__init__(parent)
        self.master.title("Cleaning Robot")
        self.screen_width = self.master.winfo_screenwidth()
        self.screen_height = self.master.winfo_screenheight()
        self.master.geometry("%dx%d+0+0" % (self.screen_width, self.screen_height))
        self.create_room_pic()
        self.create_label_room()
        self.create_departure_robot()
        self.create_arrival_robot()
        self.create_text_labels()
        self.setup_reset_button()
        
    def setup_reset_button(self):
        self.reset_button = tk.Button(self.master, text="Reset", command=self.reset)
        self.reset_button.place(x=1700, y=500)
        
    def drag_image_label2(self, event):
        self.label2.place(x=pyautogui.position().x, y=pyautogui.position().y - 100)

    # Ugly, but we need to define a function like this for the bind <B1-Motion>
    def drag_image_label3(self, event):
        self.label3.place(x=pyautogui.position().x, y=pyautogui.position().y - 100)
        
    def create_departure_robot(self):
        self.dep_robot = Image.open("robot.bmp")
        self.dep_robot_pic = ImageTk.PhotoImage(self.dep_robot)
        self.resized_dep_robot = self.dep_robot.resize((int(self.dep_robot_pic.width() / self.diff),
                                                        int(self.dep_robot_pic.height() / self.diff)), Image.ANTIALIAS)
        self.resized_dep_robot_pic = ImageTk.PhotoImage(self.resized_dep_robot)
        self.label2 = Label(self, image=self.resized_dep_robot_pic)
        self.label2.image = self.resized_dep_robot_pic
        self.label2.place(x=self.resized_room_pic.width() + 100, y=100)
        self.label2.bind("<B1-Motion>", self.drag_image_label2)

    def create_arrival_robot(self):
        self.arr_robot = Image.open("robot.bmp")
        self.arr_robot_pic = ImageTk.PhotoImage(self.arr_robot)
        self.resized_arr_robot = self.arr_robot.resize((int(self.arr_robot_pic.width() / self.diff), int(self.arr_robot_pic.height() / self.diff)), Image.ANTIALIAS)
        self.resized_arr_robot_pic = ImageTk.PhotoImage(self.resized_arr_robot)
        self.label3 = Label(self, image=self.resized_arr_robot_pic)
        self.label3.image = self.resized_arr_robot_pic
        self.pos_x = self.resized_room_pic.width() + 100
        self.label3.place(x=self.resized_room_pic.width() + 100, y=200)
        self.label3.bind("<B1-Motion>", self.drag_image_label3)
        
        
    def create_label_room(self):
        self.label1 = Label(self, image=self.resized_room_pic)
        self.label1.image = self.resized_room_pic
        self.label1.place(x=0, y=0)

    def create_text_labels(self):
        self.label4 = tk.Label(self, text="Starting point robot",
                               wraplength = 100,
                               justify = tk.CENTER)
        self.label4.place(x=self.pos_x + 80, y=100)
        self.label5 = tk.Label(self, text="Arrival point robot",
                               wraplength = 100,
                               justify = tk.CENTER)
        self.label5.place(x=self.resized_room_pic.width() + 180, y=200)


    def create_room_pic(self):
        self.room = Image.open("Room.bmp")
        self.room_pic = ImageTk.PhotoImage(self.room)
        self.width_diff = self.room_pic.width() / self.screen_width
        self.height_diff = self.room_pic.height() / self.screen_height
        self.diff = max(self.width_diff, self.height_diff)
        self.resized_room_pic = ImageTk.PhotoImage(self.room.resize((
            int(self.room_pic.width() / self.diff), int(self.room_pic.height() / self.diff)),
                                                                    Image.ANTIALIAS))
        
    def reset(self):
        self.label2.place(x=self.resized_room_pic.width() + 100, y=100)
        self.label3.place(x=self.resized_room_pic.width() + 100, y=200)




if __name__ == "__main__":
    root = tk.Tk()
    main = CleaningRobot(root)
    main.pack(fill="both", expand=True)
    root.mainloop()
