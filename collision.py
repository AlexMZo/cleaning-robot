#!/usr/env/python3

# cr: cleaning robot
# robot picture size: 48x48
# plan picture size: 1350x980


def cost_func():
    return

def check_collision_line(cr, points):
    for point in points:#x0 y1
        tested_point = (point[0]) + (point[1] * 1350)
        if tested_point > len(cr.room_pixels):
            return True
        if (cr.room_pixels[tested_point] < (250, 250, 250)):
            return True
    return False

def check_collision(cr, cr_x, cr_y):
    cursor = cr_x + (cr_y * 1350)
    collision = False
    y = 0
    while (y < 48):
        x = 0
        while (x < 48):
            tested_point = cursor + x + (y * 1350)
            if (cr.room_pixels[tested_point] < (240, 240, 240)):
                collision = True
            x = x + 1
        y = y + 1;
    if collision == True:
        cr.collision_text['text'] = "COLLISION"
    else:
        cr.collision_text['text'] = ""

def check_collision_cspace(cr, cr_x, cr_y):
    y = 0
    while (y < 48):
        x = 0
        while (x < 48):
            tested_point = cr_x + x + (y * 1350 + cr_y)
            # print(tested_point)
            if (cr.room_pixels[tested_point] < (240, 240, 240)):
                return True
            x = x + 1
        y = y + 1;
    return False

def check_collision_samples(cr, s):
    if (s == ()):
        return False
    if (s[0] <= 48 or s[1] <= 48):
        return True
    y = 0
    base_point = (s[1] - 24) + ((s[0] - 24) * 1350)
    while (y < 48):
        x = 0
        while (x < 48):
            tested_point = base_point + x + (y * 1350)
            if (cr.room_pixels[tested_point] < (240, 240, 240)):
                return True
            x = x + 1
        y = y + 1;
    return False


def check_collision_v2(cr, cr_x, cr_y):
    cursor = cr_x + (cr_y * 1350)
    collision = False
    y = 0
    while (y < 48):
        x = 0
        while (x < 48):
            tested_point = cursor + x + (y * 1350)
            if (cr.room_pixels[tested_point] < (240, 240, 240)):
                collision = True
            x = x + 1
        y = y + 1;
